package com.sonu.volley;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import androidx.appcompat.app.AppCompatActivity;

public class PhotoDetail extends AppCompatActivity {
    TextView textView;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setting title in another activity

        this.setTitle("PhotoDetail");
        //showing back button
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);

        textView=findViewById(R.id.titlep);
        imageView=findViewById(R.id.imagep);

        Intent intent=getIntent();
        Bundle args=intent.getBundleExtra("BUNDLE");
        Photo photo=(Photo) args.getSerializable("photo");
        String title=photo.getTitle();
        String url=photo.getUrl();
        textView.setText(title);
        Glide.with(getApplicationContext()).load(url).into(imageView);



    }
    //implementation of back click
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}

