package com.sonu.volley;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final static String api_url="http://jsonplaceholder.typicode.com/photos";
    ArrayList<Photo> photoArrayList;
    RecyclerView rvPhotos;
    RelativeLayout mainRL;
    Button btn1, btn2;
    NetworkChangeReceiver networkChangeReceiver=new NetworkChangeReceiver();


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            unregisterReceiver(networkChangeReceiver);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }


    public void registerBroadcaster(){
        IntentFilter filter=new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver,filter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvPhotos=findViewById(R.id.rvPhotos);
        mainRL=findViewById(R.id.relative);
        btn1=findViewById(R.id.retry);
        btn2=findViewById(R.id.notifyMe);


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet();
            }
        });


        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerBroadcaster();
            }
        });

        checkInternet();

    }

    private void checkInternet(){
        if (Utility.isDeviceOnline(getApplicationContext())){
            fetchPhotos();
            rvPhotos.setVisibility(View.VISIBLE);
            mainRL.setVisibility(View.GONE);
        }
        else{
            rvPhotos.setVisibility(View.GONE);
            mainRL.setVisibility(View.VISIBLE);
        }

    }




    private void fetchPhotos(){
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(api_url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                photoArrayList=new ArrayList<>();
                int length = response.length();
                for (int i = 0; i < length; i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        Integer id = jsonObject.getInt("id");
                        String title = jsonObject.getString("title");
                        String url = jsonObject.getString("url");
                        String thumbnailUrl = jsonObject.getString("thumbnailUrl");
                        Log.d("TAG", "onResponse:Title "+ title);

                        Photo photo=new Photo(id, title, url, thumbnailUrl);
                        photoArrayList.add(photo);

                    } catch (Exception e) {
                            e.printStackTrace();
                    }
                }

                //creating adapter passing in the data
                PhotosAdapter adapter=new PhotosAdapter(photoArrayList);

                //Attaching the adapter to the recycler view to populate items

                rvPhotos.setAdapter(adapter);
                ItemClickSupport.addTo(rvPhotos).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Intent intent=new Intent(MainActivity.this,PhotoDetail.class);
                        Photo photo=photoArrayList.get(position);
                        Log.d("TAG", "Photo data: "+photo.toString());

                        Bundle args=new Bundle();
                        args.putSerializable("photo",(Serializable)photo);
                        intent.putExtra("BUNDLE",args);
                        startActivity(intent);

                    }
                });

                rvPhotos.setLayoutManager(new LinearLayoutManager(MainActivity.this));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
    });
        requestQueue.add(jsonArrayRequest);
    }
}
