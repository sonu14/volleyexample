package com.sonu.volley;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())){
            boolean noConnection=intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY,false);

            if (noConnection){
                Toast.makeText(context, "No Internet", Toast.LENGTH_SHORT).show();
            }
            else{
                showNotification(context);
                Toast.makeText(context, "Internet available", Toast.LENGTH_SHORT).show();
            }
        }
    }


    /**
     * Create and show a simple notification and intent to MainActivity.
     */
    public static void showNotification(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("myChannelId", "My Channel", importance);
            channel.setDescription("Volley Example App");
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }

        // Builder class for devices targeting API 26+ requires a channel ID
        Intent intent = new Intent(context, MainActivity.class);
        int requestID = (int) System.currentTimeMillis();
        int flags = PendingIntent.FLAG_CANCEL_CURRENT;
        PendingIntent pendingIntent = PendingIntent.getActivities(context, requestID, new Intent[]{intent}, flags);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "myChannelId")
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle("Your connection is back")
                .setContentText("Visit Volley Example to explore more")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setVibrate(new long[]{1000, 1000})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }
}
