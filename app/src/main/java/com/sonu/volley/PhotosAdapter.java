package com.sonu.volley;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {
    Context context;
    private List<Photo> photoList;

    public PhotosAdapter(List<Photo>photos){
        photoList=photos;
    }

    @NonNull
    @Override
    public PhotosAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        LayoutInflater inflater= LayoutInflater.from(context);
        //inflate view
        View PhotoView=inflater.inflate(R.layout.item_photo,parent,false);
        //return a new holder instance

        ViewHolder viewHolder=new ViewHolder(PhotoView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PhotosAdapter.ViewHolder holder, int position) {
        //Get the data model based on position.
        Photo photo=photoList.get(position);

        //set item views based on our view and data models
        TextView titleTv=holder.titleTV;
        titleTv.setText(photo.getTitle());

        ImageView imageView=holder.imageView;
        Glide.with(context).load(photo.getThumbnailUrl()).placeholder(R.drawable.ic_check).into(imageView);
    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView titleTV;
        public ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTV=itemView.findViewById(R.id.title);
            imageView=itemView.findViewById(R.id.imageV);
        }
    }
}
